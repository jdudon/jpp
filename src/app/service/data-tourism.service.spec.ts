import { TestBed } from '@angular/core/testing';

import { DataTourismService } from './data-tourism.service';

describe('DataTourismService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataTourismService = TestBed.get(DataTourismService);
    expect(service).toBeTruthy();
  });
});
