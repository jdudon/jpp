import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from '~/environements/environment';
import { InterestPoint } from '../entity/interest-point';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataTourismService {

  private url = environment.serverUrl;

  constructor(private http: HttpClient) { }


  findAll(): Observable<any> {

    return this.http.get(this.url);
  }

  // find(data, id:number): Observable<any> {
  //   return this.http.get(data + 'single/' + id);
  // }
}
