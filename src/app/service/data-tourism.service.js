"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var environment_1 = require("~/environements/environment");
var DataTourismService = /** @class */ (function () {
    function DataTourismService(http) {
        this.http = http;
        this.url = environment_1.environment.serverUrl;
    }
    DataTourismService.prototype.findAll = function () {
        return this.http.get(this.url);
    };
    DataTourismService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], DataTourismService);
    return DataTourismService;
}());
exports.DataTourismService = DataTourismService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS10b3VyaXNtLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJkYXRhLXRvdXJpc20uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEyQztBQUMzQyw2Q0FBaUQ7QUFDakQsMkRBQTBEO0FBTzFEO0lBSUUsNEJBQW9CLElBQWdCO1FBQWhCLFNBQUksR0FBSixJQUFJLENBQVk7UUFGNUIsUUFBRyxHQUFHLHlCQUFXLENBQUMsU0FBUyxDQUFDO0lBRUksQ0FBQztJQUd6QyxvQ0FBTyxHQUFQO1FBRUUsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBVlUsa0JBQWtCO1FBSDlCLGlCQUFVLENBQUM7WUFDVixVQUFVLEVBQUUsTUFBTTtTQUNuQixDQUFDO3lDQUswQixpQkFBVTtPQUp6QixrQkFBa0IsQ0FlOUI7SUFBRCx5QkFBQztDQUFBLEFBZkQsSUFlQztBQWZZLGdEQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCdcbmltcG9ydCB7IGVudmlyb25tZW50IH0gZnJvbSAnfi9lbnZpcm9uZW1lbnRzL2Vudmlyb25tZW50JztcbmltcG9ydCB7IEludGVyZXN0UG9pbnQgfSBmcm9tICcuLi9lbnRpdHkvaW50ZXJlc3QtcG9pbnQnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBEYXRhVG91cmlzbVNlcnZpY2Uge1xuXG4gIHByaXZhdGUgdXJsID0gZW52aXJvbm1lbnQuc2VydmVyVXJsO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cENsaWVudCkgeyB9XG5cblxuICBmaW5kQWxsKCk6IE9ic2VydmFibGU8YW55PiB7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwLmdldCh0aGlzLnVybCk7XG4gIH1cblxuICAvLyBmaW5kKGRhdGEsIGlkOm51bWJlcik6IE9ic2VydmFibGU8YW55PiB7XG4gIC8vICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQoZGF0YSArICdzaW5nbGUvJyArIGlkKTtcbiAgLy8gfVxufVxuIl19