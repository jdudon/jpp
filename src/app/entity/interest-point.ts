export interface InterestPoint {
  id?:number;
  name:string;
  tarification:string;
  address:string;
  coordinate?: number[];
  type?:string;
}
