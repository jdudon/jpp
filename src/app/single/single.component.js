"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var data_tourism_service_1 = require("../service/data-tourism.service");
var router_1 = require("nativescript-angular/router");
var operators_1 = require("rxjs/operators");
var SingleComponent = /** @class */ (function () {
    function SingleComponent(pageRoute, service) {
        this.pageRoute = pageRoute;
        this.service = service;
    }
    SingleComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.pageRoute.activatedRoute.pipe(operators_1.switchMap(function (ActivatedRoute) { return ActivatedRoute.queryParams; })).subscribe(function (data) { return _this.interestPoint = data; });
    };
    SingleComponent = __decorate([
        core_1.Component({
            selector: 'ns-single',
            templateUrl: './single.component.html',
            styleUrls: ['./single.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [router_1.PageRoute, data_tourism_service_1.DataTourismService])
    ], SingleComponent);
    return SingleComponent;
}());
exports.SingleComponent = SingleComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2luZ2xlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNpbmdsZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBa0Q7QUFDbEQsd0VBQXFFO0FBQ3JFLHNEQUF3RDtBQUd4RCw0Q0FBMkM7QUFRM0M7SUFFRSx5QkFBb0IsU0FBbUIsRUFBVSxPQUEyQjtRQUF4RCxjQUFTLEdBQVQsU0FBUyxDQUFVO1FBQVUsWUFBTyxHQUFQLE9BQU8sQ0FBb0I7SUFBSSxDQUFDO0lBRWpGLGtDQUFRLEdBQVI7UUFBQSxpQkFRRDtRQU5HLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLElBQUksQ0FDaEMscUJBQVMsQ0FBQyxVQUFBLGNBQWMsSUFBRyxPQUFBLGNBQWMsQ0FBQyxXQUFXLEVBQTFCLENBQTBCLENBQUMsQ0FFdkQsQ0FBQyxTQUFTLENBQ1QsVUFBQyxJQUFJLElBQUssT0FBQSxLQUFJLENBQUMsYUFBYSxHQUFHLElBQUksRUFBekIsQ0FBeUIsQ0FDcEMsQ0FBQztJQUNOLENBQUM7SUFaWSxlQUFlO1FBTjNCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsV0FBVztZQUNyQixXQUFXLEVBQUUseUJBQXlCO1lBQ3RDLFNBQVMsRUFBRSxDQUFDLHdCQUF3QixDQUFDO1lBQ3JDLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtTQUNwQixDQUFDO3lDQUc4QixrQkFBUyxFQUFtQix5Q0FBa0I7T0FGakUsZUFBZSxDQWEzQjtJQUFELHNCQUFDO0NBQUEsQUFiRCxJQWFDO0FBYlksMENBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRGF0YVRvdXJpc21TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZS9kYXRhLXRvdXJpc20uc2VydmljZSc7XG5pbXBvcnQgeyBQYWdlUm91dGUgfSBmcm9tICduYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgSW50ZXJlc3RQb2ludCB9IGZyb20gJy4uL2VudGl0eS9pbnRlcmVzdC1wb2ludCc7XG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBzd2l0Y2hNYXAgfSBmcm9tIFwicnhqcy9vcGVyYXRvcnNcIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbnMtc2luZ2xlJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3NpbmdsZS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL3NpbmdsZS5jb21wb25lbnQuY3NzJ10sXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG59KVxuZXhwb3J0IGNsYXNzIFNpbmdsZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIGludGVyZXN0UG9pbnQ6YW55O1xuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHBhZ2VSb3V0ZTpQYWdlUm91dGUsIHByaXZhdGUgc2VydmljZTogRGF0YVRvdXJpc21TZXJ2aWNlKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICBcbiAgICB0aGlzLnBhZ2VSb3V0ZS5hY3RpdmF0ZWRSb3V0ZS5waXBlKFxuICAgICAgc3dpdGNoTWFwKEFjdGl2YXRlZFJvdXRlID0+QWN0aXZhdGVkUm91dGUucXVlcnlQYXJhbXMpXG4gICAgXG4gICAgKS5zdWJzY3JpYmUoXG4gICAgICAoZGF0YSkgPT4gdGhpcy5pbnRlcmVzdFBvaW50ID0gZGF0YSAgICAgIFxuICAgICk7XG59XG59XG4iXX0=